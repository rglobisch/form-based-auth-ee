package org.example.views;

import org.example.model.User;
import org.example.repository.UserRepository;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

@Named
@SessionScoped
public class UserManager implements Serializable{

  @Inject
  private UserRepository userRepo;

  private User currentUser;

  public boolean isSignedIn() {
    return currentUser != null;
  }

  public User getCurrentUser() {
    return currentUser;
  }

  public void setUser(User user){
    currentUser = user;
  }

//  public String signIn(String username, String password) {
//    User user = userService.getUser(username);
//    if (user == null || !password.equals(user.getPassword())) {
//      FacesContext.getCurrentInstance().addMessage(null,
//          new FacesMessage("Please enter a valid username and password."));
//      return "failure";
//    }
//
//    currentUser = user;
//    return "success";
//  }

  public void editUser() {
    String summary = "Welcome " + currentUser.getUserName();
    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
    FacesContext.getCurrentInstance().addMessage(null, message);
  }


  public void logOut() throws IOException {
    // End the session, removing any session state, including the current user and content of the shopping cart
    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
    ec.invalidateSession();
    ec.redirect("../admin/protected.xhtml");
  }

//  public String logOut() {
//    // End the session, removing any session state, including the current user and content of the shopping cart
//    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//    ec.invalidateSession();
//
////    try {
////      ec.redirect("index2.xhtml");
////    } catch (IOException e) {
////      e.printStackTrace();
////    }
//    return "/index2.xhtml?faces-redirect=true";
//  }

//  public String save(User user) {
//    userRepo.saveUser(user);
//    currentUser = user;
//    return "home";
//  }
}
