package org.example.filters;

import org.example.model.User;
import org.example.repository.UserRepository;
import org.example.views.UserManager;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * https://stackoverflow.com/questions/14758429/accessing-user-details-after-logging-in-with-java-ee-form-authentication/14758549#14758549
 * This filter sets the user name after the form-based authentication.
 */
@WebFilter("/admin/*")
public class UserFilter implements Filter {

  @Inject
  private UserRepository userRepo;

  @Inject
  private UserManager userManager;

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)  {
    HttpServletRequest request = (HttpServletRequest) req;
    String remoteUser = request.getRemoteUser();

    if (remoteUser != null) {
      HttpSession session = request.getSession();

      if (session.getAttribute("user") == null) {
        User user = userRepo.find(remoteUser);
        session.setAttribute("user", user);
        userManager.setUser(user);
      }
    }

    try {
      chain.doFilter(req, res);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ServletException e) {
      e.printStackTrace();
    }
    int i = 0;
  }

  @Override
  public void destroy() {

  }
}
