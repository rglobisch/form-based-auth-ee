Insert into Codec (id, codec, implementation) values(1001, 'H264', 'VPP');
Insert into Codec (id, codec, implementation) values(1003, 'H264', 'OpenH264');
Insert into Codec (id, codec, implementation) values(1002, 'H264', 'X264');
Insert into Codec (id, codec, implementation) values(1004, 'AAC', 'faac');
Insert into Codec (id, codec, implementation) values(1005, 'AAC', 'MainConcept');
Insert into Codec (id, codec, implementation) values(1006, 'AMR', 'libamr');

Insert into User(id, user_name, password) values ('1', 'admin-user', '5f4dcc3b5aa765d61d8327deb882cf99');
Insert into User(id, user_name, password) values ('2', 'api-user', '5f4dcc3b5aa765d61d8327deb882cf99');
Insert into User(id, user_name, password) values ('3', 'guest-user', '5f4dcc3b5aa765d61d8327deb882cf99');
Insert into Role(id, user_fk, role, role_group) values('1', '1', 'admin', 'Roles');
Insert into Role(id, user_fk, role, role_group) values('2', '1', 'user', 'Roles');
Insert into Role(id, user_fk, role, role_group) values('3', '1', 'guest', 'Roles');
Insert into Role(id, user_fk, role, role_group) values('4', '2', 'user', 'Roles');
Insert into Role(id, user_fk, role, role_group) values('5', '2', 'guest', 'Roles');
Insert into Role(id, user_fk, role, role_group) values('6', '3', 'guest', 'Roles');
