package org.example.repository;

import org.example.model.Codec;

import javax.annotation.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

import static javax.transaction.Transactional.TxType.REQUIRED;
import static javax.transaction.Transactional.TxType.SUPPORTS;

@Transactional(SUPPORTS)
public class CodecRepository {

  @PersistenceContext(unitName = "memoryPU")
  private EntityManager em;

  public List<Codec> findAll() {
    TypedQuery<Codec> query = em.createQuery("Select c from Codec c Order by c.codec DESC", Codec.class);
    return query.getResultList();
  }

  public Long countAll() {
    TypedQuery<Long> query = em.createQuery("SELECT COUNT(c) FROM Codec c", Long.class);
    return query.getSingleResult();
  }

  public Codec find(@NotNull @Min(1) Long id) {
    return em.find(Codec.class, id);
  }

  @Transactional(REQUIRED)
  public Codec create(@NotNull Codec codec) {
    em.persist(codec);
    return codec;
  }

  @Transactional(REQUIRED)
  public void delete(@NotNull @Min(1) Long id) {
    em.remove(em.getReference(Codec.class, id));
  }
}
