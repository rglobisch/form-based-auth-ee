package org.example.repository;

import org.example.model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import java.util.List;

import static javax.transaction.Transactional.TxType.REQUIRED;
import static javax.transaction.Transactional.TxType.SUPPORTS;

@ApplicationScoped
@Transactional(SUPPORTS)
public class UserRepository {

  @PersistenceContext(unitName = "memoryPU")
  private EntityManager em;

  public List<User> findAll() {
    TypedQuery<User> query = em.createQuery("Select u from User u Order by u.userName ASC", User.class);
    return query.getResultList();
  }

  public User find(@NotNull @Min(1) Long id) {
    return em.find(User.class, id);
  }

  public User find(@NotNull String userName) {
    TypedQuery<User> query = em.createQuery("SELECT u FROM User u where u.userName = :userName", User.class);
    query.setParameter("userName", userName);
    return query.getSingleResult();
  }

  @Transactional(REQUIRED)
  public User create(@NotNull User User) {
    em.persist(User);
    return User;
  }

  @Transactional(REQUIRED)
  public void delete(@NotNull @Min(1) Long id) {
    em.remove(em.getReference(User.class, id));
  }

}
