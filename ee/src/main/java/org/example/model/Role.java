package org.example.model;

import javax.persistence.*;

@Entity
public class Role {
  @Id
  @GeneratedValue
  private Long id;
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "user_fk")
  private User user;
  private String role;
  @Column(name = "role_group")
  private String roleGroup;

  public Role() {
  }

  public Role(User user, String role, String roleGroup) {
    this.user = user;
    this.role = role;
    this.roleGroup = roleGroup;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getRoleGroup() {
    return roleGroup;
  }

  public void setRoleGroup(String roleGroup) {
    this.roleGroup = roleGroup;
  }
}
