package org.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Codec {
  @Id
  @GeneratedValue
  private Long id;

  @Column(length = 200)
  @NotNull
  @Size(min = 1, max = 50)
  private String codec;

  @Column(length = 200)
  @NotNull
  @Size(min = 1, max = 50)
  private String implementation;

  public Codec() {
  }

  public Codec(String codec, String implementation) {
    this.codec = codec;
    this.implementation = implementation;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodec() {
    return codec;
  }

  public void setCodec(String codec) {
    this.codec = codec;
  }

  public String getImplementation() {
    return implementation;
  }

  public void setImplementation(String implementation) {
    this.implementation = implementation;
  }

  @Override
  public String toString() {
    return "Codec{" +
        "id=" + id +
        ", codec='" + codec + '\'' +
        ", implementation='" + implementation + '\'' +
        '}';
  }
}
