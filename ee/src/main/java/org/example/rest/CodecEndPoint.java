package org.example.rest;

import org.example.model.Codec;
import org.example.repository.CodecRepository;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.validation.constraints.Min;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

@ManagedBean
@Path("/codecs")
public class CodecEndPoint {

  @Inject
  private CodecRepository codecRepo;

  @GET
  @Produces(APPLICATION_JSON)
  public Response getCodecs() {
    List<Codec> codecs = codecRepo.findAll();
    if (codecs.size() == 0)
      return Response.noContent().build();

    return Response.ok(codecs).build();
  }

  @GET
  @Path("/{id : \\d+}")
  @Produces(APPLICATION_JSON)
  public Response getCodec(@PathParam("id") @Min(1) Long id) {
    Codec codec = codecRepo.find(id);
    if (codec != null) {
      return Response.ok(codec).build();
    } else {
      return Response.status(Response.Status.NOT_FOUND).build();
    }
  }

  @POST
  @Consumes(APPLICATION_JSON)
  public Response createCodec(Codec codec, @Context UriInfo uriInfo) {
    codec = codecRepo.create(codec);
    URI createdURI = uriInfo.getBaseUriBuilder().path(codec.getId().toString()).build();
    return Response.created(createdURI).build();
  }

  @DELETE
  @Path("/{id : \\d+}")
  public Response deleteCodec(@PathParam("id") @Min(1) Long id) {
    codecRepo.delete(id);
    return Response.noContent().build();
  }

  @GET
  @Path("/count")
  @Produces(TEXT_PLAIN)
  public Response countBooks() {
    Long nbOfCodecs = codecRepo.countAll();

    if (nbOfCodecs == 0)
      return Response.status(Response.Status.NO_CONTENT).build();

    return Response.ok(nbOfCodecs).build();
  }
}
